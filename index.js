// Lesson 3
function fizzBuzz() {
    let temp=[];
    for(let i=1; i<100; i++){
        if((i%3==0) && (i%5==0)){
            temp.push("FizzBuzz");
        }
        else if (i%3==0){
            temp.push("Fizz")      
        }
        else if (i%5==0){
            temp.push("Buzz")    
        }
        else{
            temp.push(`${i}`);  
        }
    }
        return temp;
}


//////////////////////////////////////////////////////////////////
function filterArray(arr){
    let output = []
    for (let i=0; i<arr.length;i++){
        if (Array.isArray(arr[i])){
            output.push(arr[i])
        }
    }
    
    let a = Array.from(new Set([].concat(...output)))
    
    // for sorting
    return a.sort()
}



// Output DON'T TOUCH!
// console.log(fizzBuzz())
console.log(filterArray([ [2], 23, 'dance', true, [3, 5, 3] ]));
